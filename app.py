from pathlib import Path

import json
from flask import Flask, render_template, send_from_directory, url_for, redirect
from flask_frozen import Freezer

app = Flask(__name__)
app.debug = True
app.config['FREEZER_BASE_URL'] = 'https://jnewberry0502.gitlab.io/'
app.config['FREEZER_DESTINATION'] = 'public'
freezer = Freezer(app)

projects_data = None
experiences_data = None
freelance_data = None
profiles_data = None

with open('info.json') as info_file:
    info = json.load(info_file)
    projects_data = info["projects"]
    experiences_data = info["experience"]
    freelance_data = info["freelance"]
    profiles_data = info["profiles"]

def group_generator(group, title, link_base):
    return {
        "title": title,
        "link_base": link_base,
        "groups": group
    }

@freezer.register_generator
def page_generator():
    for id, project in projects_data.items():
        project["id"] = id
        yield "project", {"id": id}
    for id, experience in experiences_data.items():
        experience["id"] = id
        yield "experience", {"id": id}
    for id, freelance in freelance_data.items():
        freelance["id"] = id
        yield "freelance", {"id": id}

@app.route('/')
def index():
    return render_template("index.html", page_title="Home")

@app.route('/projects/<id>/')
def project(id):
    project = projects_data[id]
    return render_template("project-layout.html", page_title= "Project", **(project))

@app.route('/experience/<id>/')
def experience(id):
    experience = experiences_data[id]
    return render_template("experience-layout.html", page_title= "Experience", **(experience))

@app.route('/freelance/<id>/')
def freelance(id):
    freelance = freelance_data[id]
    return render_template("freelance-layout.html", page_title= "Freelance Projects", **(freelance))

@app.route('/projects/')
def projects():
    return render_template("panel-layout.html",
        groups = [group_generator(projects_data.values(), "Projects", "projects")], page_title="Projects")

@app.route('/freelance/')
def freelances():
    return render_template("freelance-page.html",
        groups = [group_generator(freelance_data.values(), "Freelance Projects", "freelance")], profiles = profiles_data.values(), page_title="Freelance Projects")

@app.route('/experience/')
def experiences():
    return render_template("panel-layout.html",
        groups = [group_generator(experiences_data.values(), "Experience", "experience")], page_title="Experience")

@app.route('/images/<filename>')
def images(filename):
    return send_from_directory('images', filename)

@app.route('/resume.pdf')
def resume():
    return send_from_directory('other', "resume.pdf")

@app.route('/ads.txt')
def ads():
    return send_from_directory('other', "ads.txt")

@app.route('/robots.txt')
def robots():
    return send_from_directory('other', "robots.txt")

@app.route('/contact/')
def contact():
    return render_template('contact.html')

"""
@app.route('/favicon.ico')
def favicon():
    return images("favicon.ico")
"""

freezer.freeze()
freezer.run(host="0.0.0.0", port=7001)